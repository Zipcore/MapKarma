#define PLUGIN_AUTHOR ".#Zipcore"
#define PLUGIN_NAME "Map Karma"
#define PLUGIN_VERSION "1.0"
#define PLUGIN_DESCRIPTION "Rating plugin for maps"
#define PLUGIN_URL "zipcore.net"

public Plugin myinfo = 
{
	name = PLUGIN_NAME,
	author = PLUGIN_AUTHOR,
	description = PLUGIN_DESCRIPTION,
	version = PLUGIN_VERSION,
	url = PLUGIN_URL
}

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <smlib>
#include <cstrike>
#include <csgocolors>
#include <map_workshop_functions> // Adds workshop map support

#undef REQUIRE_PLUGIN

#include <zcore/zcore_mysql> // Handles listen overrides
bool g_pZcoreMysql = false;

/* Database */

bool g_bConnected;
bool g_bMapLoaded;

/* Karma */

char g_sKarma[][] =  {"+++ Fantastic", "++ Great", "+ Good", "- Bad", "-- Poor", "--- Waste"};
int g_iKarma[6] = {0, ...};

/* Macros */

#define LoopClients(%1) for(int %1 = 1; %1 <= MaxClients; %1++)

#define LoopIngameClients(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1))

#define LoopIngamePlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1) && !IsFakeClient(%1))

#define LoopAlivePlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1) && IsPlayerAlive(%1))


/* Handle Plugins */

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	RegPluginLibrary("map-karma");
	__pl_zcore_mysql__SetNTVOptional();
	
	return APLRes_Success;
}

public void OnLibraryAdded(const char[] name)
{
	if (StrEqual(name, "zcore-mysql"))
		g_pZcoreMysql = true;
}

public void OnLibraryRemoved(const char[] name)
{
	if (StrEqual(name, "zcore-mysql"))
		g_pZcoreMysql = false;
}

/* Plugin Start */

public void OnPluginStart()
{
	/* Plugins */
	
	g_pZcoreMysql = LibraryExists("zcore-mysql");
	
	/* Hooks */
	
	HookEvent("cs_win_panel_match", Event_MatchOver);
	
	/* Commands */
	
	RegAdminCmd("sm_karmalist", Cmd_KarmaAll, ADMFLAG_BAN);
	RegConsoleCmd("sm_karma", Cmd_Karma);
}

public void OnAllPluginsLoaded()
{
	/* Mysql */
	ConnectDB();
}

public void OnMapStart()
{
	if(g_bConnected && !g_bMapLoaded)
		LoadMap();
}

public void OnMapEnd()
{
	g_bMapLoaded = false;
}

/* CSGO Events */

public Action Event_MatchOver(Handle event, const char[] name, bool dontBroadcast)
{
	CreateTimer(3.0, Timer_Voteall, _, TIMER_FLAG_NO_MAPCHANGE);
	
	return Plugin_Continue;
}

public Action Timer_Voteall(Handle timer, any data)
{
	LoopIngameClients(i)
	{
		if(IsFakeClient(i) || IsClientSourceTV(i))
			continue;
		
		Menu_Vote(i);
	}
	
	return Plugin_Handled;
}

/* Database */

public void ZCore_Mysql_OnDatabaseError (int index, char[] config, char[] error )
{
	if(!StrEqual(config, "mapkarma"))
		return;
	
	DatabaseError("ZCore_Mysql_OnDatabaseError", error);
}

void DatabaseError(const char[] sFunction, const char[] error)
{
	LogError("[mapkarma.smx] SQL Error on %s: %s", sFunction, error);
	
	g_iSQL = -1;
	g_hSQL = null;
	g_bConnected = false;
}

public void ZCore_Mysql_OnDatabaseConnected(int index, char[] config, Database connection_handle)
{
	if(!StrEqual(config, "mapkarma"))
		return;
	
	g_iSQL = index;
	g_hSQL = connection_handle;
	CreateTables();
}

void ConnectDB()
{
	if(!g_pZcoreMysql)
		return;
		
	if(g_hSQL != null)
	{
		g_bConnected = true;
		return;
	}
	
	ZCore_Mysql_Connect("mapkarma");
	
	if(g_hSQL != null)
	{
		g_bConnected = true;
		CreateTables();
	}
}

void CreateTables()
{
	SQL_TQuery(g_hSQL, CallBack_CreateTables, "CREATE TABLE IF NOT EXISTS `mapkarma` (`map` varchar(64) NOT NULL PRIMARY KEY, `karma1` int(11) NOT NULL,`karma2` int(11) NOT NULL,`karma3` int(11) NOT NULL,`karma4` int(11) NOT NULL,`karma5` int(11) NOT NULL,`karma6` int(11) NOT NULL);");
}

public void CallBack_CreateTables(Handle owner, Handle hndl, const char[] error, any userid)
{
	if (owner == null || hndl == null)
	{
		DatabaseError("CallBack_CreateTables", error);
		return;
	}
	
	g_bConnected = true;
	
	LoadMap();
}

void ResetKarma()
{
	g_iKarma[0] = 0;
	g_iKarma[1] = 0;
	g_iKarma[2] = 0;
	g_iKarma[3] = 0;
	g_iKarma[4] = 0;
	g_iKarma[5] = 0;
}

int GetTotalVotes()
{
	int iTotal = g_iKarma[0] + g_iKarma[1] + g_iKarma[2] + g_iKarma[3] + g_iKarma[4] + g_iKarma[5];
	
	if(iTotal == 0)
		return 0;
	
	return iTotal;
}

int GetKarma()
{
	int iKarma = RoundToNearest(((float(g_iKarma[0])+float(g_iKarma[1])*0.8+float(g_iKarma[2])*0.6+float(g_iKarma[3])*0.4+float(g_iKarma[4])*0.2)/GetTotalVotes())*100.0);
	
	if(iKarma < 0)
		iKarma = 100
	
	return iKarma;
}

void LoadMap()
{
	g_bMapLoaded = false;
	ResetKarma();
	
	char sRawMap[PLATFORM_MAX_PATH];
	char sMap[64];
	GetCurrentMap(sRawMap, sizeof(sRawMap));
	RemoveMapPath(sRawMap, sMap, sizeof(sMap));
	
	char sQuery[512];
	Format(sQuery, sizeof(sQuery), "SELECT `karma1`, `karma2`, `karma3`, `karma4`, `karma5`, `karma6` FROM `mapkarma` WHERE `map` = '%s';", sMap);
	
	SQL_TQuery(g_hSQL, CallBack_LoadMap, sQuery);
}

public void CallBack_LoadMap(Handle owner, Handle hndl, const char[] error, any userid)
{
	if (owner == null || hndl == null)
	{
		DatabaseError("CallBack_LoadMap", error);
		return;
	}
	
	if(SQL_GetRowCount(hndl) && SQL_FetchRow(hndl))
	{
		g_iKarma[0] = SQL_FetchInt(hndl, 0);
		g_iKarma[1] = SQL_FetchInt(hndl, 1);
		g_iKarma[2] = SQL_FetchInt(hndl, 2);
		g_iKarma[3] = SQL_FetchInt(hndl, 3);
		g_iKarma[4] = SQL_FetchInt(hndl, 4);
		g_iKarma[5] = SQL_FetchInt(hndl, 5);
		
		g_bMapLoaded = true;
	}
	else
	{
		char sRawMap[PLATFORM_MAX_PATH];
		char sMap[64];
		GetCurrentMap(sRawMap, sizeof(sRawMap));
		RemoveMapPath(sRawMap, sMap, sizeof(sMap));
		
		char sQuery[512];
		Format(sQuery, sizeof(sQuery), "INSERT IGNORE INTO `mapkarma`(`map`, `karma1`, `karma2`, `karma3`, `karma4`, `karma5`, `karma6`) VALUES ('%s',0,0,0,0,0,0);", sMap);
		
		SQL_TQuery(g_hSQL, CallBack_InsertMap, sQuery);
	}
}

public void CallBack_InsertMap(Handle owner, Handle hndl, const char[] error, any userid)
{
	if (owner == null || hndl == null)
	{
		DatabaseError("CallBack_InsertMap", error);
		return;
	}
}

void Menu_Vote(int iClient)
{
	Menu menu = new Menu(MenuHandler_Vote);
	menu.SetTitle("How would you rate this map?\nCurrent Karma: %d (%d votes)\n \nMaps under a value of 50\nwill be dismissed!\n ", GetKarma(), GetTotalVotes());
	
	char sInfo[11], sBuffer[128];
	for (int i = 0; i <= 5; i++)
	{
		IntToString(i, sInfo, sizeof(sInfo));
		Format(sBuffer, sizeof(sBuffer), "%s (%d)", g_sKarma[i], g_iKarma[i]);
		menu.AddItem(sInfo, sBuffer);
	}
	
	//menu.AddItem("menu","BLABLA", ITEMDRAW_DISABLED)

	SetMenuExitButton(menu, true);
	
	menu.Display(iClient, 10);
}

public int MenuHandler_Vote(Menu menu, MenuAction action, int iClient, int params)
{
	if (action == MenuAction_Select)
	{
		char sInfo[64];
		menu.GetItem(params, sInfo, sizeof(sInfo));
		AddVote(iClient, StringToInt(sInfo));
	}
	else if (action == MenuAction_End)
		delete menu;
}

void AddVote(int client, int karma)
{
	if(!g_bConnected || !g_pZcoreMysql || !client || !IsClientInGame(client) || IsFakeClient(client))
		return;
	
	char sRawMap[PLATFORM_MAX_PATH];
	char sMap[64];
	GetCurrentMap(sRawMap, sizeof(sRawMap));
	RemoveMapPath(sRawMap, sMap, sizeof(sMap));
	
	char sQuery[512];
	FormatEx(sQuery, sizeof(sQuery), "UPDATE `mapkarma` SET `karma%d` = `karma%d`+1 WHERE `map` = '%s';", karma+1, karma+1, sMap);
	SQL_TQuery(g_hSQL, CallBack_AddVote, sQuery);	
}

public void CallBack_AddVote(Handle owner, Handle hndl, const char[] error, any userid)
{
	if (owner == null || hndl == null)
	{
		DatabaseError("CallBack_AddVote", error);
		return;
	}
}

/* Commands */

public Action Cmd_Karma(int iClient, int iArgs)
{
	if(!iClient || !IsClientInGame(iClient) || IsFakeClient(iClient))
		return Plugin_Handled;
	
	if(!g_bConnected || !g_pZcoreMysql || !g_bMapLoaded)
	{
		CPrintToChat(iClient, "{darkblue}[☯MAP-KARMA☯] {darkred}Karma not loaded yet");
		return Plugin_Handled;
	}
	
	char sRawMap[PLATFORM_MAX_PATH];
	char sMap[64];
	GetCurrentMap(sRawMap, sizeof(sRawMap));
	RemoveMapPath(sRawMap, sMap, sizeof(sMap));
	
	int iKarma = GetKarma();
	char sColor[64];
	
	if(iKarma < 50)
		sColor = "{darkred}";
	else if(iKarma < 60)
		sColor = "{yellow}";
	else if(iKarma < 90)
		sColor = "{green}";
	else sColor = "{blue}";
	
	CPrintToChat(iClient, "{darkblue}[☯MAP-KARMA☯] {grey}Karma of {purple}%s{grey}: %s%d {grey}with %d votes", sMap, sColor, iKarma, GetTotalVotes());
	
	return Plugin_Handled;
}

/* Admin Commands */

public Action Cmd_KarmaAll(int iClient, int iArgs)
{
	if(!g_bConnected || !g_pZcoreMysql)
	{
		PrintToConsole(iClient, "SQL not connected yet ?!?");
		return Plugin_Handled;
	}
	
	SQL_TQuery(g_hSQL, CallBack_LoadStats, "SELECT *, ((`karma1`+`karma2`*0.8+`karma3`*0.6+`karma4`*0.4+`karma5`*0.2)/(`karma1`+`karma2`+`karma3`+`karma4`+`karma5`+`karma6`))*100.0 as karma FROM `mapkarma` ORDER BY karma DESC;", iClient);
	
	return Plugin_Handled;
}

public void CallBack_LoadStats(Handle owner, Handle hndl, const char[] error, any iClient)
{
	if (owner == null || hndl == null)
	{
		DatabaseError("CallBack_LoadStats", error);
		return;
	}
	
	int iCount = SQL_GetRowCount(hndl);
	
	
	PrintToConsole(iClient, "---------------------------------------------------------------------------");
	PrintToConsole(iClient, "| MAP-KARMA SYSTEM ~ Idea and Logic: DrunkenMonkey | Programmer: Zipcore ~ |");
	PrintToConsole(iClient, "---------------------------------------------------------------------------");
	PrintToConsole(iClient, "| Found %d maps |", iCount);
	PrintToConsole(iClient, "--------------------------------------------------------------------------------------------------------");
	
	
	while(SQL_FetchRow(hndl))
	{
		// id 	map 	karma1 	karma2 	karma3 	karma4 	karma5 	karma6 	karma Absteigend 1
		
		char sMap[64];
		SQL_FetchString(hndl, 0, sMap, sizeof(sMap));
		
		int iKarma[6];
		float fKarma;
		
		iKarma[0] = SQL_FetchInt(hndl, 1);
		iKarma[1] = SQL_FetchInt(hndl, 2);
		iKarma[2] = SQL_FetchInt(hndl, 3);
		iKarma[3] = SQL_FetchInt(hndl, 4);
		iKarma[4] = SQL_FetchInt(hndl, 5);
		iKarma[5] = SQL_FetchInt(hndl, 6);
		
		fKarma = SQL_FetchFloat(hndl, 7);
		
		PrintToConsole(iClient, "%s:\n			%d	| +++: %d	++: %d	+: %d	-: %d	--: %d	---: %d	Total: %d	|", 
							sMap, RoundToNearest(fKarma), iKarma[0], iKarma[1], iKarma[2], iKarma[3], iKarma[4], iKarma[5], iKarma[0]+iKarma[1]+iKarma[2]+iKarma[3]+iKarma[4]+iKarma[5]);
	}
	PrintToConsole(iClient, "--------------------------------------------------------------------------------------------------------");
	
	CPrintToChat(iClient, "{darkblue}[☯MAP-KARMA☯] {grey}See output in console!");
}